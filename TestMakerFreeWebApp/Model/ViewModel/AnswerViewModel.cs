﻿using System;
using System.ComponentModel;

namespace TestMaker.Models.ViewModel
{
    public class AnswerViewModel
    {
        #region Constructor
        public AnswerViewModel()
        {
        }
        #endregion
        #region Properties
        public long Id { get; set; }
        public long QuestionId { get; set; }
        public long QuizId { get; set; }
        public string Text { get; set; }
        [DefaultValue(0)]
        public int Value { get; set; }
        public string Notes { get; set; }
        [DefaultValue(0)]
        public int Type { get; set; }
        [DefaultValue(0)]
        public int Flags { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        #endregion
    }
}
