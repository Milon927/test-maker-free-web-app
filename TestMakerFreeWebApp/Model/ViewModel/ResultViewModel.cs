﻿using System;
using System.ComponentModel;

namespace TestMaker.Models.ViewModel
{
    public class ResultViewModel
    {
        #region Constructor
        public ResultViewModel()
        {
        }
        #endregion

        #region Properties
        public long Id { get; set; }
        public long QuizId { get; set; }
        public string Text { get; set; }
        public string Notes { get; set; }
        [DefaultValue(0)]
        public int Type { get; set; }
        [DefaultValue(0)]
        public int Flags { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        #endregion
    }
}
