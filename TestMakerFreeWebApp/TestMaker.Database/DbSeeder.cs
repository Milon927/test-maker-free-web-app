﻿using System;
using Microsoft.EntityFrameworkCore.Internal;
using TestMaker.Models.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace TestMaker.Database
{
    public static class DbSeeder
    {
        #region Public Methods
        public static void Seed(TestMakerDbContext dbContext)
        {
            // Create default Users (if there are none)
            if (!dbContext.Users.Any())
                CreateUsers(dbContext);
            /* Create default Quizzes (if there are none) together with their
            set of Q & A*/
            if (!dbContext.Quizzes.Any())
                CreateQuizzes(dbContext);
        }
        #endregion

        #region Seed Methods

        private static void CreateUsers(TestMakerDbContext dbContext)
        {
            // local variables
            DateTime createDate = new DateTime(2020, 01, 01, 00, 00, 00);
            DateTime lastModifiedDate = DateTime.Now;
            // create the "Admin" ApplicationUser account (if it doesn't exist already)
            var userAdmin = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Admin",
                Email = "admin@gmail.com",
                CreatedDate = createDate,
                LastModifiedDate = lastModifiedDate
            };
            // Insert the Admin user into Database
            dbContext.Users.Add(userAdmin);
            
#if DEBUG
            var userMilon = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Milon",
                Email = "milon@gmail.com",
                CreatedDate = createDate,
                LastModifiedDate = lastModifiedDate
            };
            var userSolice = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Solice",
                Email = "solice@testmakerfree.com",
                CreatedDate = createDate,
                LastModifiedDate = lastModifiedDate
            };
            var userVodan = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Vodan",
                Email = "vodan@testmakerfree.com",
                CreatedDate = createDate,
                LastModifiedDate = lastModifiedDate
            };
            // Insert sample registered user into Database
            dbContext.Users.AddRange(userMilon, userSolice, userVodan);
#endif
            dbContext.SaveChanges();
        }

        private static void CreateQuizzes(TestMakerDbContext dbContext)
        {
            // local variables
            DateTime createdDate = new DateTime(2016, 03, 01, 12, 30, 00);
            DateTime lastModifiedDate = DateTime.Now;
            // retrieve the admin user, which we'll use as default author.
            var authorId = dbContext.Users.FirstOrDefault(u => u.UserName == "Admin")?.Id;

#if DEBUG
            // create 47 sample quizzes with auto-generated data
            // (including questions, answers & results) 
            var num = 47;
            for (int i = 1; i <= num; i++)
            {
                CreateSampleQuiz(dbContext, i, authorId, num - i, 3, 3, 3, createdDate.AddDays(-num));
            }
#endif
            // create 3 more quizzes with better descriptive data
            // (we'll add the questions, answers & results later on)
            EntityEntry<Quiz> q1 = dbContext.Quizzes.Add(new Quiz()
            {
                UserId = authorId,
                Title = "Are you more Light or Dark side of the Force?",
                Description = "Star Wars personality test",
                Text = @"Choose wisely you must, young padawan: " +
                       "this test will prove if your will is strong enough " +
                       "to adhere to the principles of the light side of the Force " +
                        "or if you're fated to embrace the dark side. " +
                        "No you want to become a true JEDI, you can't possibly miss this!",
                ViewCount = 2343,
                CreatedDate = createdDate,
                LastModifiedDate = lastModifiedDate
            });
            EntityEntry<Quiz> q2 = dbContext.Quizzes.Add(new Quiz()
            {
                UserId = authorId,
                Title = "GenX, GenY or Genz?",
                Description = "Find out what decade most represents you",
                Text = @"Do you feel confortable in your generation? " +
                       "What year should you have been born in?" +
                       "Here's a bunch of questions that will help you to find out!",
                ViewCount = 4180,
                CreatedDate = createdDate,
                LastModifiedDate = lastModifiedDate
            });
            EntityEntry<Quiz> q3 = dbContext.Quizzes.Add(new Quiz()
            {
                UserId = authorId,
                Title = "Which Shingeki No Kyojin character are you?",
                Description = "Attack On Titan personality test",
                Text = @"Do you relentlessly seek revenge like Eren? " +
                       "Are you willing to put your like on the stake to protect your friends like Mikasa ? " +
                       "Would you trust your fighting skills like Levi " +
                        "or rely on your strategies and tactics like Arwin? " +
                        "Unveil your true self with this Attack On Titan personality test!",
                ViewCount = 5203,
                CreatedDate = createdDate,
                LastModifiedDate = lastModifiedDate
            });
            // persist the changes on the Database
            dbContext.SaveChanges();
        }
        #endregion

        #region Utilites
        // <summary>
        // Creates a sample quiz and add it to the Database
        // together with a sample set of questions, answers & results.
        // </summary>
        // <param name="userId">the author ID</param>
        // <param name="id">the quiz ID</param>
        // <param name="createdDate">the quiz CreatedDate</param>
        private static void CreateSampleQuiz(
            TestMakerDbContext dbContext,
            int num,
            string authorId,
            int viewCount,
            int numberOfQuestions,
            int numberOfAnswersPerQuestion,
            int numberOfResults,
            DateTime createdDate)
        {
            var quiz = new Quiz()
            {
                UserId = authorId,
                Title = $"Quiz {num} Title",
                Description = $"This is a sample description for quiz {num}.",
                Text = "This is a sample quiz created by the DbSeeder class for testing purposes. " +
                       "All the questions, answers & results are auto-generated as well.",
                ViewCount = viewCount,
                CreatedDate = createdDate,
                LastModifiedDate = createdDate
            };
            dbContext.Quizzes.Add(quiz);
            dbContext.SaveChanges();
            for (int i = 0; i < numberOfQuestions; i++)
            {
                var question = new Question()
                {
                    QuizId = quiz.Id,
                    Text = "This is a sample question created by the DbSeeder class for testing purposes. " +
                            "All the child answers are auto-generated as well.",
                    CreatedDate = createdDate,
                    LastModifiedDate = createdDate
                };
                dbContext.Questions.Add(question);
                dbContext.SaveChanges();
            

                for (int i2 = 0; i2 < numberOfAnswersPerQuestion; i2++)
                {
                    var e2 = dbContext.Answers.Add(new Answer()
                    {
                        QuestionId = question.Id,
                        Text = "This is a sample answer created by the DbSeeder class for testing purposes. ",
                        Value = i2,
                        CreatedDate = createdDate,
                        LastModifiedDate = createdDate
                    });
                }
            }

            for (int i = 0; i < numberOfResults; i++)
            {
                dbContext.Results.Add(new Result()
                {
                    QuizId = quiz.Id,
                    Text = "This is a sample result created by the DbSeeder class for testing purposes. ",
                    MinValue = 0,
                    // max value should be equal to answers number * max answer
                    MaxValue = numberOfAnswersPerQuestion * 2,
                    CreatedDate = createdDate,
                    LastModifiedDate = createdDate
                });
            }
            dbContext.SaveChanges();
        }
        #endregion
    }
}
