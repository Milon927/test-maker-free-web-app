import { Component, OnInit } from '@angular/core';
import { Quiz } from 'src/app/models/quiz';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-quiz-edit',
  templateUrl: './quiz-edit.component.html',
  styleUrls: ['./quiz-edit.component.css']
})
export class QuizEditComponent implements OnInit {
  title: string;
  quiz: Quiz;
  // this will be TRUE when editing an existing quiz,
  // FALSE when creating a new one.
  editMode: boolean;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private quizService: QuizService) {
    // create an empty object from the Quiz interface
    this.quiz = new Quiz();

   }

  ngOnInit(): void {
    let id = +this.activatedRoute.snapshot.params['id'];
    if (id) {
      this.editMode = true;
      // fetch the quiz from the server
      this.quizService.GetQuiz(id).subscribe(result => {
        this.quiz = result;
        this.title = 'Edit - ' + this.quiz.title;
      }, error => {
        console.error(error);
      });
    } else {
      this.editMode = false;
      this.title = 'Create a new Quiz';
      }
  }
  onSubmit(quiz: Quiz) {
    if (this.editMode) {
      this.quizService.UpdateQuiz(quiz).subscribe(result => {
        console.log('Quiz ' + result.id + ' has been updated.');
    this.router.navigate(['home']);
      }, error => console.log(error));
    } else {
      this.quizService.AddQuiz(quiz).subscribe(result => {
        console.log('Quiz ' + result.id + ' has been created.');
      this.router.navigate(['home']);
      }, error => console.log(error));
    }
  }
  onBack() {
    this.router.navigate(['home']);
    }
}
