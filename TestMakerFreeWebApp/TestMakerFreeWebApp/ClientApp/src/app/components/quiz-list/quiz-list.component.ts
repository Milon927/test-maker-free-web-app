import { Component, OnInit, Input } from '@angular/core';
import { QuizListService } from '../../services/quiz-list.service';
import { Quiz } from '../../models/quiz';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.css']
})
export class QuizListComponent implements OnInit {
  @Input() class: string;
  title: string;
  selectedQuiz: Quiz;
  quizzes: Quiz[];
  constructor(private quizListService: QuizListService, private router: Router) {

  }
  ngOnInit() {
    this.quizListService.GetAll(this.class).subscribe(result => {
      this.quizzes = result;
    }, error => console.error(error));
  }
  onSelect(quiz: Quiz) {
    this.selectedQuiz = quiz;
    console.log('quiz with Id '
    + this.selectedQuiz.id
    + ' has been selected.');
    this.router.navigate(['quiz', this.selectedQuiz.id]);
    }

}
