import { Component, OnInit, Input } from '@angular/core';
import { Quiz } from '../../models/quiz';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../../services/quiz.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  quiz: Quiz;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private quizService: QuizService) {
   // this.quiz = new Quiz();
  }

  ngOnInit(): void {
    let id = +this.activatedRoute.snapshot.params['id'];
    console.log(id);
    if (id) {
    // TO-DO: load the quiz using server- side API
    this.quizService.GetQuiz(id).subscribe(result => {
      this.quiz = result;
    }, error => console.error(error));
    } else {
    console.log('Invalid id: routing back to home...');
    this.router.navigate(['home']);
 }
  }
  onEdit() {
    this.router.navigate(['quiz/edit', this.quiz.id]);
  }
  onDelete() {
    if (confirm('Do you really want to delete this quiz?')) {
      this.quizService.DeleteQuiz(this.quiz.id).subscribe(res => {
      console.log('Quiz ' + this.quiz.id + ' has been deleted.');
      this.router.navigate(['home']);
      }, error => console.log(error));
    }
  }
}
