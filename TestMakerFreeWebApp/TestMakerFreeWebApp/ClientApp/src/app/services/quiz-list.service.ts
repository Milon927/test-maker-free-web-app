import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Quiz } from '../models/quiz';
const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  )
};
@Injectable({ providedIn: 'root'})
export class QuizListService {

  url: string;
  constructor(@Inject('BASE_URL') baseUrl: string, private http: HttpClient) {
  this.url = baseUrl + 'api/quiz/';
}
  GetAll(type: string): Observable<Quiz[]> {
    return this.http.get<Quiz[]>(this.url + type + '/');
  }
}
