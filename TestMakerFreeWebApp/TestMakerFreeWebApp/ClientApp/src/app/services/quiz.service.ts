import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Quiz } from '../models/quiz';

@Injectable({providedIn: 'root'})
export class QuizService {
  url: string;
  constructor(@Inject('BASE_URL') private baseUrl: string, private http: HttpClient) {
    this.url = this.baseUrl + 'api/Quiz/';
  }
  GetQuiz(id: number): Observable<Quiz> {
    return this.http.get<Quiz>(this.url + id);
  }
  AddQuiz(quiz: Quiz): Observable<Quiz> {
    return this.http.put<Quiz>(this.url, quiz);
  }
  UpdateQuiz(quiz: Quiz): Observable<Quiz> {
    return this.http.post<Quiz>(this.url, quiz);
  }
  DeleteQuiz(id: number) {
    return this.http.delete(this.url + id);
  }
}
