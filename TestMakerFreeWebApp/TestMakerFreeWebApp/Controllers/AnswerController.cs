﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestMaker.Models.Model;

namespace TestMakerFreeWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        // GET: api/Answer
        [HttpGet("All/{questionId}")]
        public IActionResult All(int questionId)
        {
            List<Answer> sampleAnswers = new List<Answer>
            {
                // add a first sample answer
                new Answer()
                {
                    Id = 1,
                    QuestionId = questionId,
                    Text = "Friends and family",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                }
            };
            // add a bunch of other sample answers
            for (int i = 2; i <= 5; i++)
            {
                sampleAnswers.Add(new Answer()
                    {
                        Id = i,
                        QuestionId = questionId,
                        Text = $"Sample Answer {i}",
                        CreatedDate = DateTime.Now,
                        LastModifiedDate = DateTime.Now
                });
            }
            return Ok(sampleAnswers);
        }
        #region RESTful conventions methods
        /// <summary>
        /// Retrieves the Answer with the given {id}
        /// </summary>
        /// &lt;param name="id">The ID of an existing Answer</param>
        /// <returns>the Answer with the given {id}</returns>
        // GET: api/Answer/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Content("Not implemented (yet)!");
        }

        /// <summary>
        /// Adds a new Answer to the Database
        /// </summary>
        /// <param name="m">The Answer containing the data to insert</param>
        // POST: api/Answer
        [HttpPost]
        public IActionResult Post(Answer m)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Edit the Answer with the given {id}
        /// </summary>
        /// <param name="m">The Answer containing the data to update</param>
        // PUT: api/Answer/5
        [HttpPut]
        public IActionResult Put(Answer m)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Deletes the Answer with the given {id} from the Database
        /// </summary>
        /// <param name="id">The ID of an existing Answer</param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
