﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestMaker.Models.Model;

namespace TestMakerFreeWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        // GET: api/Question
        [HttpGet("All/{quizId}")]
        public IActionResult All(int quizId)
        {
            List<Question> sampleQuestions = new List<Question>
            {
                // add a first sample question
                new Question()
                {
                    Id = 1,
                    QuizId = quizId,
                    Text = "What do you value most in your life?",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                }
            };
            // add a bunch of other sample questions
            for (int i = 2; i <= 5; i++)
            {
                sampleQuestions.Add(new Question()
                {
                    Id = i,
                    QuizId = quizId,
                    Text = $"Sample Question {i}",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                });
            }
            return Ok(sampleQuestions);
        }

        #region RESTful conventions methods
        /// <summary>
        /// Retrieves the Question with the given {id}
        /// </summary>
        /// &lt;param name="id">The ID of an existing Question</param>
        /// <returns>the Question with the given {id}</returns>
        // GET: api/Question/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Content("Not implemented (yet)!");
        }

        /// <summary>
        /// Adds a new Question to the Database
        /// </summary>
        /// <param name="m">The Question containing the data to insert</param>
        // POST: api/Question
        [HttpPost]
        public IActionResult Post(Question m)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Edit the Question with the given {id}
        /// </summary>
        /// <param name="m">The Question containing the data to update</param>
        // PUT: api/Question/5
        [HttpPut]
        public IActionResult Put(Question m)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Deletes the Question with the given {id} from the Database
        /// </summary>
        /// <param name="id">The ID of an existing Question</param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
