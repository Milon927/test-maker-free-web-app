﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using TestMaker.Database;
using TestMaker.Models.Model;
using TestMaker.Models.ViewModel;

namespace TestMakerFreeWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : ControllerBase
    {
        #region Private Fields
        private TestMakerDbContext _dbContext;
        #endregion

        #region Constructor
        public QuizController(TestMakerDbContext context)
        {
            // Instantiate the ApplicationDbContext through DI
            _dbContext = context;
        }
        #endregion Constructor

        // GET: api/Quiz
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            // for demo data
            //var quizObject = Latest() as OkObjectResult;
            //var allQuiz = quizObject.Value as List<QuizViewModel>;
            // return Ok(allQuiz.FirstOrDefault(c => c.Id == id));
            // from database
            var quiz = _dbContext.Quizzes.FirstOrDefault(c => c.Id == id);
            if (quiz == null)
            {
                return NotFound(new
                {
                    Error = $"Quiz ID {id} has not been found"
                });
            }
            return Ok(quiz.Adapt<QuizViewModel>());
        }

        // GET: api/Quiz/5
        [HttpGet("Latest/{num:int?}")]
        public IActionResult Latest (int num = 10)
        {
            // for demo data 
            /*
            List<QuizViewModel> sampleQuizzes = new List<QuizViewModel>
            {
                new QuizViewModel()
                {
                    Id = 1,
                    Title = "Which Shingeki No Kyojin character are you?",
                    Description = "Anime-related personality test",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                }
            };
            // add a first sample quiz
            // add a bunch of other sample quizzes
            for (int i = 2; i <= num; i++)
            {
                sampleQuizzes.Add(new QuizViewModel()
                {
                    Id = i,
                    Title = $"Sample Quiz {i}",
                    Description = "This is a sample quiz",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                });
            }
            return Ok(sampleQuizzes);
            */
            // from database
            var latestQuizzes = _dbContext.Quizzes.OrderByDescending(c => c.CreatedDate).Take(num).ToArray();
            return Ok(latestQuizzes.Adapt<QuizViewModel[]>());
        }

        /// <summary>
        /// GET: api/quiz/ByTitle
        /// Retrieves the {num} Quizzes sorted by Title (A to Z)
        /// </summary>
        /// <param name="num">the number of quizzes to retrieve</param>
        /// <returns>{num} Quizzes sorted by Title</returns>
        [HttpGet("ByTitle/{num:int?}")]
        public IActionResult ByTitle(int num = 10)
        {
            // for demo data
            /*
            var quizes = Latest(num) as OkObjectResult; // ((JsonResult)Latest(num)).Value as List<Quiz>;
            var sampleQuizzes = quizes.Value as List<QuizViewModel>;

            return Ok(sampleQuizzes.OrderBy(t => t.Title));
            */
            // from database
            var byTitle = _dbContext.Quizzes.OrderBy(c => c.Title).Take(num).ToArray();
            return Ok(byTitle.Adapt<QuizViewModel[]>());
        }

        /// <summary>
        /// GET: api/quiz/mostViewed
        /// Retrieves the {num} random Quizzes
        /// </summary>
        /// <param name="num">the number of quizzes to retrieve</param>
        /// <returns>{num} random Quizzes</returns>
        [HttpGet("Random/{num:int?}")]
        public IActionResult Random(int num = 10)
        {
            // when return data is json data
            //var sampleQuizzes = ((JsonResult) Latest(num)).Value 
            //    as List<Quiz>;
            //when return data is IActionResult & OkObjectResult
            // for demo data 
            /*
            var quizes = Latest(num) as OkObjectResult; // ((JsonResult)Latest(num)).Value as List<Quiz>;
            var sampleQuizzes = quizes.Value as List<QuizViewModel>;
            return Ok(sampleQuizzes.OrderBy(t => Guid.NewGuid()));
            */
            // from database
            var random = _dbContext.Quizzes.OrderBy(c => Guid.NewGuid()).Take(num).ToArray();
            return Ok(random.Adapt<QuizViewModel[]>());
        }
        // <summary>
        // Edit the Quiz with the given {id}
        // </summary>
        // <param name="model">The QuizViewModel containing the data to update</param>
        // POST: api/Quiz
        [HttpPost]
        public IActionResult Post([FromBody] QuizViewModel model)
        {
            // return a generic HTTP Status 500 (Server Error)
            // if the client payload is invalid.
            if (model == null) return new StatusCodeResult(500);
            // retrieve the quiz to edit
            var quiz = _dbContext.Quizzes.FirstOrDefault(q => q.Id ==model.Id);
            // handle requests asking for non-existing quizzes
            if (quiz == null)
            {
                return NotFound(new
                {
                    Error = $"Quiz ID {model.Id} has not been found",
                });
            }
            // handle the update (without object-mapping)
            // by manually assigning the properties
            // we want to accept from the request
            quiz.Title = model.Title;
            quiz.Description = model.Description;
            quiz.Text = model.Text;
            quiz.Notes = model.Notes;
            // properties set from server-side
            quiz.LastModifiedDate = quiz.CreatedDate;
            // persist the changes into the Database.
            _dbContext.SaveChanges();
            // return the updated Quiz to the client.
            return Ok(quiz.Adapt<QuizViewModel>());
        }
        // <summary>
        // Adds a new Quiz to the Database
        // </summary>
        // <param name="model">The QuizViewModel containing the data to insert</param>
        // PUT: api/Quiz/5
        [HttpPut]
        public IActionResult Put([FromBody] QuizViewModel model)
        {
            // return a generic HTTP Status 500 (Server Error)
            // if the client payload is invalid.
            if (model == null)
                return new StatusCodeResult(500);
            // handle the insert (without object-mapping)
            var quiz = new Quiz
            {
                // properties taken from the request
                Title = model.Title,
                Description = model.Description,
                Text = model.Text,
                Notes = model.Notes
            };
            // properties set from server-side
            quiz.CreatedDate = DateTime.Now;
            quiz.LastModifiedDate = quiz.CreatedDate;
            // Set a temporary author using the Admin user's userId
            // as user login isn't supported yet: we'll change this later on.
            quiz.UserId = _dbContext.Users.FirstOrDefault(u => u.UserName == "Admin")?.Id;
            // add the new quiz
            _dbContext.Quizzes.Add(quiz);
            // persist the changes into the Database.
            _dbContext.SaveChanges();
            return Ok(quiz.Adapt<QuizViewModel>());
        }
        // <summary>
        // Deletes the Quiz with the given {id} from the Database
        // </summary>
        // <param name="id">The ID of an existing Test</param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            // retrieve the quiz from the Database
            var quiz = _dbContext.Quizzes.FirstOrDefault(c => c.Id == id);
            // handle requests asking for non-existing quizzes
            if (quiz == null)
            {
                return NotFound(new
                {
                    Error = $"Quiz ID {id} has not been found",
                });
            }
            // remove the quiz from the DbContext.
            _dbContext.Quizzes.Remove(quiz);
            // persist the changes into the Database.
            _dbContext.SaveChanges();
            // return an HTTP Status 200 (OK).
            return Ok();
        }
    }
}
