﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestMaker.Models.Model;

namespace TestMakerFreeWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultController : ControllerBase
    {
        // GET: api/Result
        [HttpGet("All/{quizId}")]
        public IActionResult All(int quizId)
        {
            List<Result> sampleResults = new List<Result>()
            {
                // add a first sample result
                new Result()
                {
                    Id = 1,
                    QuizId = quizId,
                    Text = "What do you value most in your life?",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                }
            };

            // add a bunch of other sample results
            for (int i = 2; i <= 5; i++)
            {
                sampleResults.Add(new Result()
                {
                    Id = i,
                    QuizId = quizId,
                    Text = $"Sample Question {i}",
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                });
            }
            return Ok(sampleResults);
        }

        #region RESTful conventions methods
        /// <summary>
        /// Retrieves the Result with the given {id}
        /// </summary>
        /// &lt;param name="id">The ID of an existing Result</param>
        /// <returns>the Result with the given {id}</returns>
        // GET: api/Result/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Content("Not implemented (yet)!");
        }

        /// <summary>
        /// Adds a new Result to the Database
        /// </summary>
        /// <param name="m">The Result containing the data to insert</param>
        // POST: api/Result
        [HttpPost]
        public IActionResult Post(Result m)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Edit the Result with the given {id}
        /// </summary>
        /// <param name="m">The Result containing the data to update</param>
        // PUT: api/Result/5
        [HttpPut]
        public IActionResult Put(Result m)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Deletes the Result with the given {id} from the Database
        /// </summary>
        /// <param name="id">The ID of an existing Result</param>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
